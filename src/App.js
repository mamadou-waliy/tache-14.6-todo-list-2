import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
function Todo({ todo, index, removeTodo }) {
    return (
      <div className="container" Style="background:#ddd">
        <div className="row List">
          <div className="col-md-11">
              <div className="Item">
                {todo.text}
              </div>
          </div>
          <div className="col-md-1">
            <button className="btn Remove" onClick={() => removeTodo(index)}> x </button>
          </div>
        </div>
      </div>
    );
  
}

function TodoForm({ addTodo }) {
  const [value, setValue] = React.useState("");

  const handleSubmit = e => {
    e.preventDefault();
    if (!value) return;
    addTodo(value);
    setValue("");
  };
  const reset = e => {
    setValue("");
  };
  return (
    <form onSubmit={handleSubmit}>
      <div class="input-group">
          <input className="form-control" 
            type="text" placeholder="Tape something here"
            value={value}
            onChange={e => setValue(e.target.value)}
          />
          <button className="btn Btn-add" Type="submit"> Add </button>
          <button className="btn Btn-cancel" onClick={() => reset()}> Cancel </button>
      </div>
    </form>
  );
}

function App() {
  const [todos, setTodos] = React.useState([
    { text: "Learn about Programming"}
  ]);
  const addTodo = text => {
    const newTodos = [...todos, { text }];
    setTodos(newTodos);
  };
  
  const removeTodo = index => {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
  };
  
  return (
    <div className="App">
      <div className="App-todo">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="offset-md-2 Todo">
                <div className="Todo-list">
                  <h2>Todos</h2><br/>
                  <TodoForm addTodo={addTodo}></TodoForm>
                  {
                    todos.map((todo, index) => 
                    (<Todo key={index} index={index} todo={todo} removeTodo={removeTodo}/>))
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
